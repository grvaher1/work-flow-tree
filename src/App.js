import ConfigManager from "./core/pages/ConfigManager";

function App() {
  return (
    <ConfigManager/>
  );
}

export default App;
