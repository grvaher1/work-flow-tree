import React from 'react';
import { Select } from 'antd'

const SelectInputNode = (props) => {
  const {
    data,
  } = props

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };
  return (
    <Select
      defaultValue={data.label}
      style={{
        width: 150,
      }}
      onChange={handleChange}
      options={data?.options}
    />
  );
}

export default SelectInputNode;