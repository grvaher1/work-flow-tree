import React from 'react';

import { Card, Col, Row } from 'antd';

import { ERROR_TYPES } from '../../configManager.constants';

const cardStyle = {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '16px',
    height: '90%'
};

const StatusCards = (props) => {
    const {
        data
    } = props

    const { 
        RUN_TIME_ERROR,
        COMPILE_TIME_ERROR,
        PENDING
     } = data;
    return (
        <Row gutter={16} style={{ marginTop: '10px', marginBottom: '10px' }}>
            <Col span={4}>
                <Card title={RUN_TIME_ERROR} style={{ ...cardStyle, background: 'rgba(255, 0, 0, 0.2)' }}>
                    Total Runtime Error
                </Card>
            </Col>

            <Col span={4}>
                <Card title={COMPILE_TIME_ERROR} style={{ ...cardStyle, background: 'rgba(148, 0, 211, 0.2)' }}>
                    Total Design Error
                </Card>
            </Col>

            <Col span={4}>
                <Card title={PENDING} style={{ ...cardStyle, background: 'rgba(0, 0, 255, 0.2)' }}>
                    Pending Verification
                </Card>
            </Col>
        </Row>
    );
}

export default StatusCards;