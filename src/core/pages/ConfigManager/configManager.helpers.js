import _map from 'lodash/map';
import _filter from 'lodash/filter';
import _isEmpty from 'lodash/isEmpty';
import _forEach from 'lodash/forEach';

import DEPENDANCY_CONFIG_MAP from '../../mock/confiurations.map.json';

import { ERROR_TYPES } from './configManager.constants';

export const fetchConfigById = (node, visitedTargetNodes = {}, visitedSourceNodes = {}, result = [], isSource) => {
    if (!isSource && visitedTargetNodes[node]) {
        return result;
    }

    if (isSource && visitedSourceNodes[node]) {
        return result;
    }

    if (!isSource) {
        visitedTargetNodes[node] = true;
    }

    if (isSource) {
        visitedSourceNodes[node] = true;
    }

    const connections = DEPENDANCY_CONFIG_MAP.data;
    const filteredData = _filter(connections, (connection) => {
        return isSource ? connection.target === node : connection.source === node;
    });

    if (_isEmpty(filteredData)) {
        return result;
    }

    result.push(...filteredData);

    const sourceNode = filteredData?.[0]?.source;
    const targetNode = filteredData?.[0]?.target;

    result = fetchConfigById(sourceNode, visitedTargetNodes, visitedSourceNodes, result, true);
    result = fetchConfigById(targetNode, visitedTargetNodes, visitedSourceNodes, result, false);

    return result;
};


export const getEdges = (connections) => {
    return _map(connections, (connection, index) => {
        return {
            ...connection,
            id: `${index}`,
            type: 'step',
        }
    })
}

export const getVerificationStatusCount = (data) => {
    const statusCount = {
        [ERROR_TYPES.RUN_TIME_ERROR]: 0,
        [ERROR_TYPES.COMPILE_TIME_ERROR]: 0,
        [ERROR_TYPES.PENDING]: 0,
    };

    _forEach(data, (configType) => {
        _forEach(configType, (config) => {
            if (config.verificationStatus) {
                statusCount[config.verificationStatus]++;
            }
        });
    });

    return statusCount;
};

export const getNodePosition = (node, indent) => {
    if (node.configType === 'environments') {
        return { x: 100, y: indent }
    } else if (node.configType === 'features') {
        return { x: 300, y: indent }
    } else if (node.configType === 'subFeatures') {
        return { x: 500, y: indent }
    } else if (node.configType === 'categories') {
        return { x: 700, y: indent }
    } else if (node.configType === 'variables') {
        return { x: 900, y: indent }
    }
}


