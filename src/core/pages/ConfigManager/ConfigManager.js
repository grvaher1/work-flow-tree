import React, { useState, useEffect, useMemo } from 'react';
import ReactFlow from 'reactflow';

import _forEach from 'lodash/forEach';
import _map from 'lodash/map';

import ENVIRONMENTS_MOCK from '../../mock/environments.json';
import FEATURES_MOCK from '../../mock/features.json';
import SUBFEATURE_MOCK from '../../mock/subFeatures.json';
import CATEGORIES_MOCK from '../../mock/categories.json';
import CONFIG_VARIABLES_MOCK from '../../mock/variables.json';

import StatusCards from './components/statusCards/index';

import { fetchConfigById, getEdges, getVerificationStatusCount, getNodePosition } from './configManager.helpers';

import SelectInputNode from './components/selectInputNode';

import 'reactflow/dist/style.css';

const styles = {
    background: 'rgba(200, 200, 200, 0.2)',
    width: '100%',
    height: 200,
  };

const ConfigManager = () => {
    const [edges, setEdges] = useState([]);
    const [nodes, setNodes] = useState([]);
    const [statusCount, setStatusCount] = useState([]);

    const nodeTypes = useMemo(() => ({ selectInput: SelectInputNode }), []);

    useEffect(() => {
        setInitialConfig()
    }, [])

    const setInitialConfig = () => {
        const combinedConfig = [ENVIRONMENTS_MOCK, FEATURES_MOCK, SUBFEATURE_MOCK, CATEGORIES_MOCK, CONFIG_VARIABLES_MOCK];
        const countVerificationStatus = getVerificationStatusCount(combinedConfig);
        setStatusCount(countVerificationStatus)
        const combinedNodes = []
        _forEach(combinedConfig, item => {
            const config = _map(item, (element, index) => {
                const indent = index * 100;
                return {
                    type: element?.type,
                    id: element?.id, position: getNodePosition(element, indent),
                    data: {
                        label: element.title,
                    },

                    sourcePosition: 'right', targetPosition: 'left'
                }
            })
            combinedNodes.push(...config)
        })
        setNodes(combinedNodes)
    }

    const handleNodeClick = (event, node) => {
        const response = fetchConfigById(node.id)
        const edges = getEdges(response)
        setEdges(edges)
    }

    return (
        <div className='container'>
           <StatusCards data={statusCount}/>
            <div style={{ width: '100vw', height: '100vh' }}>
                <ReactFlow nodes={nodes} edges={edges}
                    onNodeClick={handleNodeClick}
                    nodeTypes={nodeTypes}
                    panOnDrag={true}
                    preventScrolling={false}
                    style={styles}
                />
            </div>
        </div>
    );
}

export default ConfigManager;